# As per: "Frequency Domain SysID for a Small, Low-Cost, Fixed WInd UAV" Dorobantu A. et all
# ref.:
# /media/Storage1/20.Nauka/00.Current/20.ControlTheory/SysID/UltrastickSYSID.pdf
#   Mass:       1.94 kg
#   Wing Spann: 1.27 m
#   Wing Area:  0.31 m^2
#   V_cruise:   19.0 m/sec (linearization reference)
# X = [u, w, q, theta]
# U = [dE, dT]
# convention:
#   w - positive UP

M_lon = [1.943    0.0     0.0     0.0;
          0.0    1.943    0.0     0.0;
          0.0     0.0   0.1444    0.0;
          0.0     0.0     0.0     1.0 ];

A_lon_prim =  [  -1.3819     1.4360     -1.2068    -19.0418;
                 -1.3612   -17.3794     34.9752     -0.6494;
            	  0.0226    -0.6631     -1.5563      0.0;
                  0.0        0.0         1.0000      0.0 ];

B_lon_prim =  [  1.1994;
                -7.1592;
               -15.1901;
                 0.0 ];

B_dE = inv(M_lon)*B_lon_prim;

A = inv(M_lon)*A_lon_prim;

# A =   -0.71122    0.73906   -0.62110   -9.80021
#       -0.70057   -8.94462   18.00062   -0.33423
#        0.15651   -4.59211  -10.77770    0.00000
#        0.00000    0.00000    1.00000    0.00000

B =  [  0.6173     0.200;
   	   -3.6846     0.0;
   	 -105.1946     0.0;
        0.0        0.0];

B(:,1) = B_dE;

C = [1.0     0.0     0.0     0.0;
     0.0     1.0     0.0     0.0;
     0.0     0.0    57.296   0.0;
     0.0     0.0     0.0    57.296];

D = [0     0
     0     0
     0     0
     0     0];

pkg load control
pkg load communications
pkg load data-smoothing

states = {'ias' 'climb speed' 'pitch rate' 'theta'};
inputs = {'elevator' 'throttle'};
outputs = {'ias' 'climb speed' 'pitch rate' 'pitch angle'};

ss_ultra = ss(A,B,C,D,'statename',states,...
            'inputname',inputs,...
            'outputname',outputs);

tf_ultra = tf(ss_ultra);
fr_ultra = frd(ss_ultra);

dt = 0.05;
sim_time = [0:0.05:15];

U_dub =     [zeros(10,1); ones(50,1); -1*ones(50,1); zeros(191,1)];
U_dub_x5 =  [zeros(10,1); 2*ones(10,1); -2*ones(10,1); zeros(271,1)];
U_none =    zeros(301,1);
U_step =    [zeros(10,1); ones(291,1)];

U_elv_dub =  [ 0.02*U_dub    U_none ];

U_thr_dub =  [ U_none        10.0*U_dub ];

U_elv_step = [ -0.01*U_step   U_none ];

figure(1);
clf();
lsim(ss_ultra, U_elv_step, sim_time)

figure(2);
clf();
lsim(ss_ultra, U_thr_dub, sim_time)

# [Y_elv, T_elv, X_elv] = lsim(ss_ultra, U_elv_dub, sim_time);
