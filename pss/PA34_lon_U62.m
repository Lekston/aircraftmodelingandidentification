% PA34/M20 longitudinal model
% state vector: [U, alpha, q, theta];
% U0 = 62m/s
A=[-0.1126    4.75    0    -9.81;
   -0.0049   -1.7576  1     0;
    0.00574 -18.8    -4.632 0;
    0         0       1     0];

B=[  0;
     0;
   -47;
     0];

C=eye(4,4);
C(2,2) = 57.296;
C(2,2) = 57.296;
C(4,4) = 57.296;

D=zeros(4,1);

pkg load control
pkg load communications
pkg load data-smoothing

states = {'ias' 'AoA' 'pitch rate' 'theta'};
inputs = {'elevator'};
outputs = {'ias' 'AoA' 'pitch rate' 'pitch angle'};

ss_pa34 = ss(A,B,C,D,'statename',states,...
            'inputname',inputs,...
            'outputname',outputs);

dt = 0.05;
sim_time = [0:0.05:30];

ss_disc_pa34 = ss(A,B,C,D, dt,'statename',states,...
                  'inputname',inputs,...
                  'outputname',outputs);

tf_pa34 = tf(ss_pa34);
fr_pa34 = frd(ss_pa34);

sim_arr_size = size(sim_time,2);

U_dub =     [zeros(10,1); ones(50,1); -1*ones(50,1); zeros(sim_arr_size - 110,1)];
U_dub_x5 =  [zeros(10,1); 2*ones(10,1); -2*ones(10,1); zeros(sim_arr_size - 30,1)];
U_none =    zeros(sim_arr_size,1);
U_step =    [zeros(10,1); ones(sim_arr_size - 10,1)];

U_elv_dub =  [ 0.02*U_dub ];

U_elv_step = [ -0.01*U_step ];

figure(3);
clf();
lsim(ss_pa34, U_elv_step, sim_time)

figure(4);
clf();
lsim(ss_pa34, U_elv_dub, sim_time)

# [Y_elv, T_elv, X_elv] = lsim(ss_pa34, U_elv_dub, sim_time);
