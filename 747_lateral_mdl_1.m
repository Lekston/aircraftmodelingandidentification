# MACH 0.8 at F400

A = [-0.0558   -0.9968    0.0802    0.0415
      0.5980   -0.1150   -0.0318         0
     -3.0500    0.3880   -0.4650         0
           0    0.0805    1.0000         0];

B = [ 0.0073         0
     -0.4750    0.0077
      0.1530    0.1430
           0         0];

C = [1     0     0     0
     0     1     0     0
     0     0     1     0
     0     0     0     1];

D = [0     0
     0     0
     0     0
     0     0];

pkg load control

states = {'beta' 'yaw_rate' 'roll_rate' 'phi'};
inputs = {'rudder' 'aileron'};
outputs = {'beta' 'yaw rate' 'roll rate' 'bank angle'};

sys_747 = ss(A,B,C,D,'statename',states,...
            'inputname',inputs,...
            'outputname',outputs);

tf_747 = tf(sys_747);
fr_747 = frd(sys_747);

dt = 0.05;
id_time = [0:0.05:15];

# roll rate and bank angle after an aileron step
# lsim(tf_747(2,2),[zeros(50,1); ones(451,1)],[0:dt:25])
# lsim(tf_747(3,2),[zeros(50,1); ones(451,1)],[0:dt:25])

# aileron doublet
# lsim(tf_747(2,2),[zeros(20,1); ones(80,1); -1*ones(80,1); zeros(321,1)],[0:dt:25])
# lsim(tf_747(3,2),[zeros(20,1); ones(80,1); -1*ones(80,1); zeros(321,1)],[0:dt:25])

# Detect model from synthetic data (747 sim output + noise)
# actuate elevator (nose up) only
# lsim(sys_747,[zeros(301,1) [zeros(20,1); 0.2*ones(80,1); zeros(201,1)]],[0:dt:15])

U_dub = [zeros(10,1); 0.2*ones(50,1); -0.2*ones(50,1); zeros(191,1)];
U_none = zeros(301,1);

U_ail = [U_none U_dub];
U_rdr = [U_dub U_none];

[Y_ail, T_ail, X_ail] = lsim(sys_747, U_ail, id_time);
[Y_rdr, T_rdr, X_rdr] = lsim(sys_747, U_rdr, id_time);

figure(1);
lsim(sys_747, U_ail, id_time)
figure(2)
lsim(sys_747, U_rdr, id_time)

##
#   Rudder to outputs
##
dat_rdr2beta = iddata(Y_rdr(:,1),U_dub,0.05);
sys_id_rdr2beta = moen4(dat_rdr2beta, 4);
tfID11 = d2c(tf(sys_id_rdr2beta));
set(tfID11, 'inname', 'rudder');
set(tfID11, 'outname', 'beta');

dat_rdr2yawr = iddata(Y_rdr(:,2),U_dub,0.05);
sys_id_rdr2yawr = moen4(dat_rdr2yawr, 4);
tfID12 = d2c(tf(sys_id_rdr2yawr));
set(tfID12, 'inname', 'rudder');
set(tfID12, 'outname', 'yaw rate');

dat_rdr2rollr = iddata(Y_rdr(:,3),U_dub,0.05);
sys_id_rdr2rollr = moen4(dat_rdr2rollr, 4);
# sys_id_rdr2rollr = moesp(dat_rdr2rollr, 4);
# sys_id_rdr2rollr = n4sid(dat_rdr2rollr, 4);
tfID13 = d2c(tf(sys_id_rdr2rollr));
set(tfID13, 'inname', 'rudder')
set(tfID13, 'outname', 'roll rate')

dat_rdr2bank = iddata(Y_rdr(:,4),U_dub,0.05);
sys_id_rdr2bank = moen4(dat_rdr2bank, 4);
tfID14 = d2c(tf(sys_id_rdr2bank));
set(tfID14, 'inname', 'rudder')
set(tfID14, 'outname', 'bank angle')

##
#   Aileron to outputs
##
dat_ail2beta = iddata(Y_ail(:,1),U_dub,0.05);
sys_id_ail2beta = moen4(dat_ail2beta, 4);
tfID21 = d2c(tf(sys_id_ail2beta));
set(tfID21, 'inname', 'aileron');
set(tfID21, 'outname', 'beta');

dat_ail2yawr = iddata(Y_ail(:,2),U_dub,0.05);
sys_id_ail2yawr = moen4(dat_ail2yawr, 4);
tfID22 = d2c(tf(sys_id_ail2yawr));
set(tfID22, 'inname', 'aileron');
set(tfID22, 'outname', 'yaw rate');

dat_ail2rollr = iddata(Y_ail(:,3),U_dub,0.05);
sys_id_ail2rollr = moen4(dat_ail2rollr, 4);
# sys_id_ail2rollr = moesp(dat_ail2rollr, 4);
# sys_id_ail2rollr = n4sid(dat_ail2rollr, 4);
tfID23 = d2c(tf(sys_id_ail2rollr));
set(tfID23, 'inname', 'aileron')
set(tfID23, 'outname', 'roll rate')

dat_ail2bank = iddata(Y_ail(:,4),U_dub,0.05);
sys_id_ail2bank = moen4(dat_ail2bank, 4);
tfID24 = d2c(tf(sys_id_ail2bank));
set(tfID24, 'inname', 'aileron')
set(tfID24, 'outname', 'bank angle')

tfID = [tfID11 tfID21; tfID12 tfID22; tfID13 tfID23; tfID14 tfID24];
figure(2)
lsim(tfID, U_ail, id_time);

# compare with the actual model:
sys_747_d = c2d(sys_747,0.05);

tfID(:,1)
tf_747(:,1)

tfID(:,2)
tf_747(:,2)

ss_ID = ss(tfID);
btamodred(ss_ID,4);

ss_ID_4 = btamodred(ss_ID,4);

A_ID = ss_ID_4.c * ss_ID_4.a * inv(ss_ID_4.c);
B_ID = ss_ID_4.c * ss_ID_4.b;
C_ID = eye(4);
D_ID = [0 0; 0 0; 0 0; 0 0];

ss_ID_norm = ss(A_ID, B_ID, C_ID, D_ID);

#other tests (when all state variables are observable):
# A_id = corr(Y_ail);
# U_id_1 = corr(Y_ail, U_ail);
# U_id_2 = corr(Y_rdr, U_rdr);
# not exactly like that...
