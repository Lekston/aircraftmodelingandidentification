# MACH 0.8 at F400

A = [-0.0558   -0.9968    0.0802    0.0415
      0.5980   -0.1150   -0.0318         0
     -3.0500    0.3880   -0.4650         0
           0    0.0805    1.0000         0];

B = [ 0.0073         0
     -0.4750    0.0077
      0.1530    0.1430
           0         0];

C = [1     0     0     0
     0     1     0     0
     0     0     1     0
     0     0     0     1];

D = [0     0
     0     0
     0     0
     0     0];

pkg load control

states = {'beta' 'yaw_rate' 'roll_rate' 'phi'};
inputs = {'rudder' 'aileron'};
outputs = {'beta' 'yaw rate' 'roll rate' 'bank angle'};

sys_747 = ss(A,B,C,D,'statename',states,...
            'inputname',inputs,...
            'outputname',outputs);

# tf_747 = tf(sys_747);
# fr_747 = frd(sys_747);

dt = 0.05;
id_time = [0:0.05:15];

# roll rate and bank angle after an aileron step
# lsim(tf_747(2,2),[zeros(50,1); ones(451,1)],[0:dt:25])
# lsim(tf_747(3,2),[zeros(50,1); ones(451,1)],[0:dt:25])

# aileron doublet
# lsim(tf_747(2,2),[zeros(20,1); ones(80,1); -1*ones(80,1); zeros(321,1)],[0:dt:25])
# lsim(tf_747(3,2),[zeros(20,1); ones(80,1); -1*ones(80,1); zeros(321,1)],[0:dt:25])

# Detect model from synthetic data (747 sim output + noise)
# actuate elevator (nose up) only
# lsim(sys_747,[zeros(301,1) [zeros(20,1); 0.2*ones(80,1); zeros(201,1)]],[0:dt:15])

U_dub = [zeros(10,1); 0.2*ones(50,1); -0.2*ones(50,1); zeros(191,1)];
U_none = zeros(301,1);

U1 = [U_none U_dub];

[Y1, T1, X1] = lsim(sys_747,U1,[0:dt:15]);
figure(1)
lsim(sys_747, U1, id_time)

U2 = [[zeros(20,1); 0.2*ones(80,1); zeros(201,1)] zeros(301,1)];
[Y2, T2, X2] = lsim(sys_747, U2, id_time);


# dat1=iddata(Y1,U1,0.05);
# dat2=iddata(Y2,U2,0.05);
# dat=merge(dat1, dat2);
# sys_id = moen4(dat1,4);
# works when run as moen4(dat1)

dat_ail2beta = iddata(Y1(:,1),U_dub,0.05);
sys_id_ail2beta = moen4(dat_ail2beta, 4)

# compare with the actual model:
sys_747_d = c2d(sys_747,0.05);

#other tests (when all state variables are observable):
# A_id = corr(Y1);
# U_id_1 = corr(Y1, U1);
# U_id_2 = corr(Y2, U2);
# not exactly like that...
