% based on kol_pio_3.m
% cuising at 36m/s (130km/h)
% X = [u, alpha, q, theta]
% U = [dE, dT]

A =  [  -0.0668     1.7616      0       -9.8066
        -0.0150     -1.5431     1.0000  0
    	0.0043      -3.9771     -1.2020 0
        0           0           1.0000  0];

B =  [  0       0.0167
   	 0.1310     0.0001
   	 6.6443       0
        0         0];

C = [1     0     0     0
     0     1     0     0
     0     0     1     0
     0     0     0     1];

D = [0     0
     0     0
     0     0
     0     0];

pkg load control
pkg load communications
pkg load data-smoothing

figure(1);
clf();
figure(2);
clf();
figure(3);
clf();
figure(4);
clf();
figure(5);
clf();
figure(6);
clf();

states = {'ias' 'alpha' 'pitch rate' 'theta'};
inputs = {'elevator' 'throttle'};
outputs = {'ias' 'alpha' 'pitch rate' 'pitch angle'};

ss_pzl110 = ss(A,B,C,D,'statename',states,...
            'inputname',inputs,...
            'outputname',outputs);

tf_pzl110 = tf(ss_pzl110);
fr_pzl110 = frd(ss_pzl110);

dt = 0.05;
id_time = [0:0.05:15];

% throttle step
% lsim(tf_pzl110(2,2),[zeros(50,1); ones(451,1)],[0:dt:25])
% lsim(tf_pzl110(3,2),[zeros(50,1); ones(451,1)],[0:dt:25])

% throttle doublet
% lsim(tf_pzl110(2,2),[zeros(20,1); ones(80,1); -1*ones(80,1); zeros(321,1)],[0:dt:25])
% lsim(tf_pzl110(3,2),[zeros(20,1); ones(80,1); -1*ones(80,1); zeros(321,1)],[0:dt:25])

% Detect model from synthetic data (pzl110 sim output + noise)
% actuate elevator (nose up) only
% lsim(ss_pzl110,[zeros(301,1) [zeros(20,1); 0.2*ones(80,1); zeros(201,1)]],[0:dt:15])

U_dub = [zeros(30,1); linspace(0.1,1,10)'; linspace(1,0.1,10)'; zeros(251,1)];
U_none = zeros(301,1);

%U_dub_x5 = [zeros(10,1); 2*ones(10,1); -2*ones(10,1); zeros(271,1)];

% TODO explore very high impact of the doublet period on the results of identification:
%   - for 10, 40, 40, 211 (at 0, 1, -1, 0) - a 25% error in ail to roll rate is observed
%   - for 10, 30, 30, 231 (at 0, 1, -1, 0) - a 300% error in ail to yaw rate is observed
%   - for 10, 20, 20, 251 (at 0, 1, -1, 0) - gives satisfactory results
%   - for 10, 10, 10, 271 (at 0, 2, -2, 0) - gives satisfactory results
%   - for 10, 10, 281 (at 0, 1, 0)         - gives satisfactory results

U_elv = [0.1*U_dub U_none];
U_thr = [U_none 100*U_dub];

[Y_elv, T_elv, X_elv] = lsim(ss_pzl110, U_elv, id_time);
[Y_thr, T_thr, X_thr] = lsim(ss_pzl110, U_thr, id_time);

% Simulate noise impact
% TODO must reduce effects of noise before moving on with the flight ident trials
doNoise = true;
Y_elv_clean = Y_elv;
U_dub_clean = U_dub;
%Y_elv(:,1) = regdatasmooth(T_elv, awgn(Y_elv_clean(:,1), 0.1, 1.0));
if (doNoise == true)

    [Y_elv(:,1), lambda1] = regdatasmooth(T_elv, awgn(Y_elv_clean(:,1), 1000, 0.02, 'linear'));
    [Y_elv(:,2), lambda2] = regdatasmooth(T_elv, awgn(Y_elv_clean(:,2), 1000, 0.02, 'linear'));
    [Y_elv(:,3), lambda3] = regdatasmooth(T_elv, awgn(Y_elv_clean(:,3), 1000, 0.02, 'linear'));
    [Y_elv(:,4), lambda4] = regdatasmooth(T_elv, awgn(Y_elv_clean(:,4), 1000, 0.02, 'linear'));
    Y_elv(:,1) = conv(fastsmooth(awgn(Y_elv_clean(:,1), 1000, 0.02, 'linear'), 15, 3, 1), [1 2 7 10 12 16 12 10 7 2 1]/70 , 'same');
    Y_elv(:,2) = conv(fastsmooth(awgn(Y_elv_clean(:,2), 1000, 0.02, 'linear'), 15, 3, 1), [1 2 7 10 12 16 12 10 7 2 1]/70 , 'same');
    Y_elv(:,3) = conv(fastsmooth(awgn(Y_elv_clean(:,3), 1000, 0.02, 'linear'), 15, 3, 1), [1 2 7 10 12 16 12 10 7 2 1]/70 , 'same');
    Y_elv(:,4) = conv(fastsmooth(awgn(Y_elv_clean(:,4), 1000, 0.02, 'linear'), 15, 3, 1), [1 2 7 10 12 16 12 10 7 2 1]/70 , 'same');
    figure(5);
    plot(T_elv(10:260), Y_elv_clean(10:260,3), '-g', T_elv(10:260), Y_elv(10:260,3), '-b',...
            T_elv(10:260), awgn(Y_elv_clean(10:260,3), 1000, 0.02, 'linear'), 'or');
    figure(6);
    subplot(4,1,1);
    plot(T_elv(10:260), Y_elv(10:260,1), '-g');
    subplot(4,1,2);
    plot(T_elv(10:260), Y_elv(10:260,2), '-b');
    subplot(4,1,3);
    plot(T_elv(10:260), Y_elv(10:260,3), '-r');
    subplot(4,1,4);
    plot(T_elv(10:260), Y_elv(10:260,4), '-k');

    Y_thr_clean = Y_thr;
    [Y_thr(:,1), lambda_t1] = regdatasmooth(T_thr, awgn(Y_thr_clean(:,1), 1000, 0.02, 'linear'),'lambda',1.0e-04);
    [Y_thr(:,2), lambda_t2] = regdatasmooth(T_thr, awgn(Y_thr_clean(:,2), 1000, 0.02, 'linear'),'lambda',1.0e-02);
    [Y_thr(:,3), lambda_t3] = regdatasmooth(T_thr, awgn(Y_thr_clean(:,3), 1000, 0.02, 'linear'),'lambda',1.0e-04);
    [Y_thr(:,4), lambda_t4] = regdatasmooth(T_thr, awgn(Y_thr_clean(:,4), 1000, 0.02, 'linear'),'lambda',1.0e-04);
    %Y_thr(:,1) = fastsmooth(awgn(Y_thr_clean(:,1), 1000, 0.02, 'linear'), 15, 3, 1);
    %Y_thr(:,2) = fastsmooth(awgn(Y_thr_clean(:,2), 1000, 0.02, 'linear'), 15, 3, 1);
    %Y_thr(:,3) = fastsmooth(awgn(Y_thr_clean(:,3), 1000, 0.02, 'linear'), 15, 3, 1);
    %Y_thr(:,4) = fastsmooth(awgn(Y_thr_clean(:,4), 1000, 0.02, 'linear'), 15, 3, 1);

    %TODO try using the dedicated lambda for each observed I/O combination 
    U_dub = regdatasmooth(T_elv, U_dub_clean,'lambda',1.0e-04);
    %U_dub = conv(fastsmooth(U_dub_clean, 15, 3, 0), [1 2 7 10 12 16 12 10 7 2 1]/70 , 'same');

    figure(7);
    plot(T_elv(10:260), U_dub_clean(10:260), '-g', T_elv(10:260), U_dub(10:260), '-b');

    % alternate filtering
    % tmp = conv(awgn(Y_elv_clean(:,3), 1000, 0.02, 'linear'),...
    %           [1 2 7 10 12 16 12 10 7 2 1]/70 , 'same');
    % tmp = conv(tmp, [1 2 7 10 12 16 12 10 7 2 1]/70 , 'same');
    % figure(7);
    % plot(T_elv, tmp);
endif


figure(1)
lsim(ss_pzl110, U_elv, id_time)
figure(2);
lsim(ss_pzl110, U_thr, id_time)

%%
%   Elevator to outputs
%%
dat_elv2ias = iddata(Y_elv(10:260,1),0.1*U_dub(10:260),0.05);
sys_id_elv2ias0 = moen4(dat_elv2ias, 4, 'noiseinput', 'v');
sys_id_elv2ias = ss(sys_id_elv2ias0.a, sys_id_elv2ias0.b(:,1), sys_id_elv2ias0.c, 0, 0.05);
tfID11 = d2c(tf(sys_id_elv2ias));
set(tfID11, 'inname', 'elevator');
set(tfID11, 'outname', 'ias');

dat_elv2alpha = iddata(Y_elv(10:260,2),0.1*U_dub(10:260),0.05);
sys_id_elv2alpha0 = moen4(dat_elv2alpha, 4, 'noiseinput', 'v');
sys_id_elv2alpha = ss(sys_id_elv2alpha0.a, sys_id_elv2alpha0.b(:,1), sys_id_elv2alpha0.c, 0, 0.05);
tfID12 = d2c(tf(sys_id_elv2alpha));
set(tfID12, 'inname', 'elevator');
set(tfID12, 'outname', 'alpha');

dat_elv2pitchr = iddata(Y_elv(10:260,3),0.1*U_dub(10:260),0.05);
sys_id_elv2pitchr0 = moen4(dat_elv2pitchr, 4, 'noiseinput', 'v');
sys_id_elv2pitchr = ss(sys_id_elv2pitchr0.a, sys_id_elv2pitchr0.b(:,1), sys_id_elv2pitchr0.c, 0, 0.05);
% sys_id_elv2pitchr = moesp(dat_elv2pitchr, 4);
% sys_id_elv2pitchr = n4sid(dat_elv2pitchr, 4);
tfID13 = d2c(tf(sys_id_elv2pitchr));
set(tfID13, 'inname', 'elevator')
set(tfID13, 'outname', 'pitch rate')

dat_elv2pitch = iddata(Y_elv(10:260,4),0.1*U_dub(10:260),0.05);
sys_id_elv2pitch0 = moen4(dat_elv2pitch, 4, 'noiseinput', 'v');
sys_id_elv2pitch = ss(sys_id_elv2pitch0.a, sys_id_elv2pitch0.b(:,1), sys_id_elv2pitch0.c, 0, 0.05);
tfID14 = d2c(tf(sys_id_elv2pitch));
set(tfID14, 'inname', 'elevator')
set(tfID14, 'outname', 'pitch angle')

%%
%   Aileron to outputs
%%
dat_thr2ias = iddata(Y_thr(10:260,1),100*U_dub(10:260),0.05);
sys_id_thr2ias0 = moen4(dat_thr2ias, 4, 'noiseinput', 'v');
sys_id_thr2ias = ss(sys_id_thr2ias0.a, sys_id_thr2ias0.b(:,1), sys_id_thr2ias0.c, 0, 0.05);
tfID21 = d2c(tf(sys_id_thr2ias));
set(tfID21, 'inname', 'throttle');
set(tfID21, 'outname', 'ias');

dat_thr2alpha = iddata(Y_thr(10:260,2),100*U_dub(10:260),0.05);
sys_id_thr2alpha0 = moen4(dat_thr2alpha, 4, 'noiseinput', 'v');
sys_id_thr2alpha = ss(sys_id_thr2alpha0.a, sys_id_thr2alpha0.b(:,1), sys_id_thr2alpha0.c, 0, 0.05);
tfID22 = d2c(tf(sys_id_thr2alpha));
set(tfID22, 'inname', 'throttle');
set(tfID22, 'outname', 'alpha');

dat_thr2pitchr = iddata(Y_thr(10:260,3),100*U_dub(10:260),0.05);
sys_id_thr2pitchr0 = moen4(dat_thr2pitchr, 4, 'noiseinput', 'v');
sys_id_thr2pitchr = ss(sys_id_thr2pitchr0.a, sys_id_thr2pitchr0.b(:,1), sys_id_thr2pitchr0.c, 0, 0.05);
% sys_id_thr2pitchr = moesp(dat_thr2pitchr, 4);
% sys_id_thr2pitchr = n4sid(dat_thr2pitchr, 4);
tfID23 = d2c(tf(sys_id_thr2pitchr));
set(tfID23, 'inname', 'throttle')
set(tfID23, 'outname', 'pitch rate')

dat_thr2pitch = iddata(Y_thr(10:260,4),100*U_dub(10:260),0.05);
sys_id_thr2pitch0 = moen4(dat_thr2pitch, 4, 'noiseinput', 'v');
sys_id_thr2pitch = ss(sys_id_thr2pitch0.a, sys_id_thr2pitch0.b(:,1), sys_id_thr2pitch0.c, 0, 0.05);
tfID24 = d2c(tf(sys_id_thr2pitch));
set(tfID24, 'inname', 'throttle')
set(tfID24, 'outname', 'pitch angle')

tfID = [tfID11 tfID21; tfID12 tfID22; tfID13 tfID23; tfID14 tfID24];
set(tfID, 'inname', inputs);
set(tfID, 'outname', outputs);


figure(3)
lsim(tfID, U_elv, id_time);
figure(4)
lsim(tfID, U_thr, id_time);

% compare with the actual model:
ss_pzl110_d = c2d(ss_pzl110,0.05);

% Uncomment lines below to compare
% tfID(:,1)
% tf_pzl110(:,1)
% tfID(:,2)
% tf_pzl110(:,2)

ss_ID = ss(tfID);
btamodred(ss_ID,4);

ss_ID_4 = btamodred(ss_ID,4);

A_ID = ss_ID_4.c * ss_ID_4.a * inv(ss_ID_4.c);
B_ID = ss_ID_4.c * ss_ID_4.b;
C_ID = eye(4);
D_ID = [0 0; 0 0; 0 0; 0 0];

ss_ID_norm = ss(A_ID, B_ID, C_ID, D_ID);
