% based on kol_pio_3.m
% cuising at 36m/s (130km/h)
% X = [u, alpha, q, theta]
% U = [dE, dT]

A =  [ -0.0668      1.7616      0      -9.8066
       -0.0150     -1.5431      1.0000  0
    	0.0043     -3.9771     -1.2020  0
        0           0           1.0000  0];

B =  [  0       0.0167
   	 0.1310     0.0001
   	 6.6443       0
        0         0];

C=eye(4,4);
C(2,2) = 57.296;
C(3,3) = 57.296;
C(4,4) = 57.296;

D = [0     0
     0     0
     0     0
     0     0];

pkg load control
% pkg load communications
% pkg load data-smoothing

figure(1);
clf();
figure(2);
clf();

states = {'ias' 'AoA' 'pitch rate' 'theta'};
inputs = {'elevator' 'throttle'};
outputs = {'ias' 'AoA' 'pitch rate' 'pitch angle'};

ss_pzl110 = ss(A,B,C,D,'statename',states,...
            'inputname',inputs,...
            'outputname',outputs);

dt = 0.05;
sim_time = [0:0.05:50];

% Discretization:
% exact solution:
% x[k+1] = exp(A*dt) + A.inv()*(exp(A*dt)-I)*B*u[k]
%
% approx.:
% exp(A*dt) = (I + A*dt/2)*(I - A*dt/2)^(-1)
%
% Method retaining matrices
%
% Omega = (eye(4) + A*dt/2.0 + A*A*dt*dt/6.0)   ... goes on as a series
% A_disc = (eye(4) + A*dt*Omega;
% B_disc = Omega*dt*B

Omega0 = (eye(4) + A*dt/2.0) * inv(eye(4) - A*dt/2.0);
Omega = (eye(4) + A*dt/2.0 + A*A*dt*dt/6.0);

A_disc = eye(4) + A*dt*Omega;
B_disc = Omega*dt*B;

ss_disc_pzl110 = ss(A_disc, B_disc, C, D, dt,'statename',states,...
                  'inputname',inputs,...
                  'outputname',outputs);

% tf_pzl110 = tf(ss_pzl110);
% fr_pzl110 = frd(ss_pzl110);

sim_arr_size = size(sim_time,2);

U_dub =     [zeros(50,1); -1*ones(20,1); ones(20,1); zeros(sim_arr_size - 90, 1)];
U_dub_x5 =  [zeros(30,1); 2*ones(10,1); -2*ones(10,1); zeros(sim_arr_size - 50, 1)];
U_dub_lin = [zeros(30,1); linspace(0.1,1,10)'; linspace(1,0.1,10)'; zeros(sim_arr_size,1)];
U_none =    zeros(sim_arr_size,1);
U_step =    [zeros(10,1); ones(sim_arr_size - 10,1)];

U_elv_dub =  [ 0.02*U_dub U_none ];
U_elv_step = [ -0.01*U_step U_none ];

U_thr_dub =  [U_none 100*U_dub ];
U_thr_step = [U_none 100*U_step ];

figure(1);
clf();
lsim(ss_pzl110, U_elv_step, sim_time)

figure(2);
clf();
%lsim(ss_disc_pzl110, U_elv_step, sim_time)
lsim(ss_pzl110, U_elv_dub, sim_time)

figure(3);
clf();
lsim(ss_pzl110, U_thr_step, sim_time)

figure(4);
clf();
%lsim(ss_disc_pzl110, U_thr_step, sim_time)
lsim(ss_pzl110, U_thr_dub, sim_time)

# [Y_elv, T_elv, X_elv] = lsim(ss_pzl110, U_elv_dub, sim_time);
