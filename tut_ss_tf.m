% src:
% /usr/share/doc/octave-control/control.pdf.gz


s = tf('s')
G = 1/(s+1)
G1 = (s+1)/(s*(s+4)*(s^2+2*s+2))
pzmap(G1)
rlocus(G1)
bode(G1)
k=1
G1_closed = 1/(1+1*G1)
pzmap(g1_close)

G3=zpk([-2],[-1-i, -1+i, -3-i, -3+i],1)
rlocus(G3)
rlocus(G3*-1)


z = tf('z', 0.02)
H0 = 0.095/(z - 0.9)


H = filt([1 -1],[1 0.9])

        1 - z^-1  
 y1:  ------------
      1 + 0.9 z^-1


lsim(H, [0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1])


H0 = filt([1],[1 -0.5])


% MIMO model (see p.17 of control.pdf.gz):
num = {[1, 5, 7],    [1];    [1, 7], [1, 5, 5]};
den = {[1, 5, 6], [1, 2]; [1, 8, 6], [1, 3, 2]};
sys = tf(num, den)

sys1 = tf([1], [1, 7])

% select graphics toolkit
graphics_toolkit fltk
graphics_toolkit gnuplot

rlocus(sys1)

linspace(...)
logspace(...)

roots([1 0 1])


sys = Boeing707
set(sys, "inname", {"thrust", "elevator"})
# state vector
# x1 - speed (deviation from 80m/s)
# x2 - AoA (deviation from 4deg)
# x3 - pitch rate (q)
# x4 - pitch angle
# actuate both: throttle (u1) and elevator (u2)
lsim(sys,[10*ones(50,2); zeros(451,2)],[0:0.01:5])

# actuate throttle only
lsim(sys,[10*ones(501,1) zeros(501,1)],[0:0.01:5])

# actuate elevator (nose up) only
lsim(sys,[zeros(501,1) -10*ones(501,1)],[0:0.01:5])
[Y, T, X] = lsim(sys,[zeros(501,1) -10*ones(501,1)],[0:0.01:5])
# plot AoA
plot(T,X(:,2),'b-')

sys_tf = tf(sys)
sys_tf(2,2)

bode(sys_tf(2,2))
rlocus(sys_tf(2,2))
pzmap(sys_tf(2,2))

# Detect model from synthetic data (747 sim output + noise)
# actuate elevator (nose up) only
lsim(sys,[zeros(501,1) [zeros(20,1); 0.2*ones(80,1); zeros(401,1)]],[0:0.05:25])
[Y, T, X] = lsim(sys,[zeros(501,1) [zeros(20,1); 0.2*ones(80,1); zeros(401,1)]],[0:0.02:25])

# actuate throttle only
lsim(sys,[[10*ones(501,1)] zeros(501,1)],[0:0.01:5])

#########################################################3
G = 5/((s+1)^2+25)
lsim(G,ones(1,101),[0:0.05:5])
lsim(G*s,ones(1,101),[0:0.05:5])

G = 5/((s+5)^2+25)
lsim(G,ones(1,101),[0:0.05:5])

G = 5/((s+10)^2+25)
lsim(G,ones(1,101),[0:0.05:5])
